# Minizinc Planning Domains
This repository contains planning domains translated to Minizinc formats.

Specifically, domains are from Fast Downward LM-Cut Problem Suite available from (http://api.planning.domains) except for 
block domain, where some of the instances were too large for our translator to handle

The selected domains do not include conditional effects and axioms.

## Increasing Horizons
Usually, planning instances are converted to CSP instances with bounded horizons (T).
All the files in this repository starts with a line specifying the horizon length as follows:

```
int: T = 1;
array[1..3] of int: n_vals = [2, 2, 2];
array[1..4] of string: name = ["(board f1 p0)", "(depart f0 p0)", "(down f1 f0)", "(up f0 f1)"];
array[1..4] of int: cost = [1, 1, 1, 1];
```

All the horizon lengths are initially set to 1. If you want to try other horizon lengths you need to modify the first lines.

## Different Encodings
### State Change Variable Models
State change variable models described in:

- Optiplan: Unifying IP-based and Graph-based Planning (https://arxiv.org/pdf/1109.2155.pdf)
